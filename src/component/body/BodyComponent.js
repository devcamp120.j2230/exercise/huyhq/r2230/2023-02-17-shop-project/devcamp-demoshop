import { Component } from "react";
import BodyCardComponent from "./BodyCardComponent";

class BodyComponent extends Component {
    render() {
        return (
            <div className="container body-content">
                <h3>Product List</h3>
                <p>Showing 1 - 9 of 24 products</p>
                <BodyCardComponent/>
            </div>
        )
    }
}

export default BodyComponent;