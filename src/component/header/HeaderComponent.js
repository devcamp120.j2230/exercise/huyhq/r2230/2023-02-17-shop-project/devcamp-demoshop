import React, { Component } from "react";
import HeaderNavComponent from "./HeaderNavComponent";
import HeaderTitleComponent from "./HeaderTitleComponent";

class HeaderComponent extends Component {
    render() {
        return (
            <React.Fragment>
                <HeaderTitleComponent />
                <HeaderNavComponent />
            </React.Fragment>
        )
    }

}

export default HeaderComponent;