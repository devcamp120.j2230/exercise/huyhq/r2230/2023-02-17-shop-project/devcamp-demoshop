import { Component } from "react";
import logo from "../../asset/images/logo.svg";

class HeaderTitleComponent extends Component{
    render(){
        return(
        <div className="container text-center">
            <div className="header-logo">
                <a href="#" width="3em">
                    <img src={logo} alt="logo"></img>
                </a>
            </div>
            <h1>React Store</h1>
            <p>Demo App Shop24h v1.0</p>
        </div>
        )
    }
}

export default HeaderTitleComponent;