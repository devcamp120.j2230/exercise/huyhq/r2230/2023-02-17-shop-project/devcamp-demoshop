import { Component } from "react";

class FooterComponent extends Component {
    render() {
        return (
            <div className="container-fuid footer-content">
                {/* Footer Info */}
                <div className="container">
                    <div className="row pt-3">
                        <div className="col-md-4 col-xl-5">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p>©  2018 . All Rights Reserved.</p>
                        </div>
                        <div className="col-md-4">
                            <h5>Contacts</h5>
                            <dl>
                                <dt>Address:</dt>
                                <dd>Kolkata, West Bengal, India</dd>
                            </dl>
                            <dl>
                                <dt>Email:</dt>
                                <dd><a href="#">info@example.com</a></dd>
                            </dl>
                            <dl>
                                <dt>Phones:</dt>
                                <dd><a href="#">+91 99999999</a> or <a href="#">+91 11111111</a></dd>
                            </dl>
                        </div>
                        <div className="col-md-4 col-xl-3">
                            <h5>Links</h5>
                            <ul>
                                <li className="mt-3"><a href="#">About</a></li>
                                <li className="mt-3"><a href="#">Projects</a></li>
                                <li className="mt-3"><a href="#">Blog</a></li>
                                <li className="mt-3"><a href="#">Contact</a></li>
                                <li className="mt-3"><a href="#">Pricing</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                {/* Footer Social */}
                <div className="row text-center footer-social">
                    <div className="col-md-3 p-3">
                        <a href="#">Facebook</a>
                    </div>
                    <div className="col-md-3 p-3">
                        <a href="#">Instagram</a>
                    </div>
                    <div className="col-md-3 p-3">
                        <a href="#">Twitter</a>
                    </div>
                    <div className="col-md-3 p-3">
                        <a href="#">Google</a>
                    </div>
                </div>
            </div>
        )
    }

}

export default FooterComponent;