import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import HeaderComponent from './component/header/HeaderComponent';
import FooterComponent from './component/footer/FooterComponent';
import BodyComponent from './component/body/BodyComponent';

function App() {
  return (
    <div className="container-fluid p-0">
      {/* Header */}
      <HeaderComponent/>
      {/* Body Component */}
      <BodyComponent/>
      {/* Footer */}
      <FooterComponent/>
    </div>
  );
}

export default App;
